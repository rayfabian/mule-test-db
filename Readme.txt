Steps to run
1. After cloning the repo, import the project from external location.
2. In the problem tab, delete all the errors if any.
3. Start h2 server and create table
4. Run mule application
5. Use postman to send request to http://localhost:9081/json


sample request
{
	"name":"ray fabian",
	"address":"Auckland"
}


To run h2 server
java -cp h2-1.4.197.jar org.h2.tools.Server

Create a table
CREATE TABLE Customer (
    CUSTOMER_ID int NOT NULL AUTO_INCREMENT,
    NAME VARCHAR(35),
	ADDRESS VARCHAR(50),
    PRIMARY KEY (CUSTOMER_ID)
);
